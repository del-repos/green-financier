# green-financier 
Green Financier 

## :pushpin: Motivations to Invest in ESG-rated Stocks:
- Long-term sustainability: ESG-rated companies tend to focus on sustainable business practices that can contribute to their long-term financial performance. By investing in these companies, you can potentially benefit from their long-term growth and success.
- Risk management: ESG ratings assess a company's exposure to environmental, social, and governance risks. By investing in companies with higher ESG ratings, you may be reducing your exposure to certain risks, such as regulatory fines or reputational damage.
- Ethical considerations: Some investors may want to invest in companies that align with their personal values and beliefs. ESG ratings provide a way to identify companies that prioritize ethical and sustainable practices.
- Performance: There is some evidence to suggest that companies with higher ESG ratings may outperform their peers in the long run. This is because companies that prioritize sustainability and good governance may be better positioned to weather economic downturns and other challenges.
- Investor demand: There is growing demand among investors for ESG investments, which has led to an increase in the availability of ESG-rated stocks. Investing in these stocks can provide exposure to companies that are well-positioned to benefit from this trend.

## :memo: Description
The Swensen Yale portfolio is an investment strategy developed by David Swensen, the Chief Investment Officer of Yale University's endowment fund. The strategy is based on the principles of modern portfolio theory and emphasizes diversification, active management, and a focus on long-term performance. The David Swensen Portfolio asset allocation looks like this:
- 30% in Total Stock Market
- 15% in International Stock Market
- 5% in Emerging Markets
- 15% in Intermediate Treasury Bonds
- 15% in Treasury Inflation-Protected Securities (TIPS)
- 20% in Real Estate Investment Trusts (REITs)

## :hammer: How to Build
```sh
To build this portfolio, we first noted down 5 of the top companies in each category, and then measured their ESG (Environmental, Social, and Governance) rating from Morningstar. After, we looked up each company on Yahoo Finance and recorded their daily historical prices from at most five years ago to recently.
```

## :alembic: Usage
```sh


```

## :microscope: Technologies
- Language(s): `go`, `html`, `css`,
- Technologies: `nginx`, `mongoDB`

### In-depth
<!-- Github -->
<details>
	<summary>Github</summary>

- Github Projects

- Github Issues

- Github Code (Source Code)

- Github CODEOWNERS

</details>

<!-- Twilio -->
<details>
	<summary>Twilio</summary>

- Send SMS

- Send SMS with Multimedia

</details>

<!-- MongoDB -->
<details>
	<summary>MongoDB Atlas</summary>
- DB

</details>

<!-- Domain.com -->
<details>
	<summary>Domain.com</summary>

- Our dope domain: https://green-financier.tech . Check it out!

- Custom Nameservers to DigitalOcean
</details>

<!-- DigitalOcean -->
<details>
	<summary>DigitalOcean</summary>
- Droplet
  - systemd service units ~ persistence on binary
  - nginx ~ reverse proxy
  - certbot ~ auto ssl protection
- Networking
  - DNS Routing
- Firewalls
  - ufw - Host firewall
</details>


## :card_file_box: Directory Explanation
```txt

```


## :blue_book: Technical Details
- Need a `.env` file in `/src` with the following fields:
```txt
# Enter values where `<>` is
TWILIO_ACCOUNT_SID="<>"
TWILIO_AUTH_TOKEN="<>"
M_PHONE="<>"
```

## :books: Resources
- [How To Deploy a Go Web Application Using Nginx on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-go-web-application-using-nginx-on-ubuntu-18-04)
