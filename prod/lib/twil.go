package lib

import (
	"fmt"
	"os"

	"github.com/twilio/twilio-go"
	twilioApi "github.com/twilio/twilio-go/rest/api/v2010"
)

// sendBasicSms: send a basic sms message to a phone number
func sendBasicSms(message, phoneNum string) {
	mPhone := os.Getenv("M_PHONE")

	client := twilio.NewRestClient()

	params := &twilioApi.CreateMessageParams{}
	params.SetTo(phoneNum)
	params.SetFrom(mPhone)
	params.SetBody(message)

	resp, err := client.Api.CreateMessage(params)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
}
