package main

import (
	"fmt"
	"os"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"strconv"
	"text/template"

	"github.com/twilio/twilio-go"
	twilioApi "github.com/twilio/twilio-go/rest/api/v2010"
)

const PORT = 9990

// init: initial .env file
func init() {
	// load .env file
	err := godotenv.Load()

	// handle err for .env file
	if err != nil {
		log.Fatal("err: no .env file found")
	}
}

type NavbarContext struct {
	Href  string
	Name string
}

func sendBasicSms(message, phoneNum string) {
	mPhone := os.Getenv("M_PHONE")

	client := twilio.NewRestClient()

	params := &twilioApi.CreateMessageParams{}
	params.SetTo(phoneNum)
	params.SetFrom(mPhone)
	params.SetBody(message)

	resp, err := client.Api.CreateMessage(params)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)
}


// handler: basic handler for the web server
func handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":	
		t := template.Must(template.ParseGlob("tmpl/*.html"))

		nav := []NavbarContext{
			{Href: "/home", Name: "Home"},
			{Href: "/templates", Name: "Templates"},
			{Href: "/about-us", Name: "About Us"},
		}

		t.ExecuteTemplate(w, "slayout", nav)
		t.ExecuteTemplate(w, "home", nil)
		t.ExecuteTemplate(w, "elayout", nil)
	case "POST":
		
		err := r.ParseForm()
		if err != nil {
			log.Fatal(err)
		}

		msg := r.Form.Get("message")
		num := r.FormValue("number")
		
		// Extract data from form fields
		//msg := r.FormValue("message")
		//num := r.FormValue("number")

		// Do something with the data
		fmt.Printf("msg: %s\n", msg)
		fmt.Printf("num: %s\n", num)

		sendBasicSms(msg, num)
		
		t := template.Must(template.ParseGlob("tmpl/*.html"))

		nav := []NavbarContext{
			{Href: "/home", Name: "Home"},
			{Href: "/templates", Name: "Templates"},
			{Href: "/about-us", Name: "About Us"},
		}
		t.ExecuteTemplate(w, "slayout", nav)
		t.ExecuteTemplate(w, "home", nil)
		t.ExecuteTemplate(w, "elayout", nil)
	}
}

   
func templatesHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseGlob("tmpl/*.html"))

	nav := []NavbarContext{
		{Href: "/home", Name: "Home"},
		{Href: "/templates", Name: "Templates"},
		{Href: "/about-us", Name: "About Us"},
	}

	templates := []string{
		"NCSU Physicians: Remainder you have an appt on 5/3/2022 at 8:00 AM with your healthcare provider.For more details please log into MyChart (mychart.ncsuhealth.org). if you are unable to keep this appt please call our main office.",
		"You’ve joined CIS 305 Fall 2023 Campbell! Next, get the Remind app to keep schoo  communication in one place: http://rmd.me/0bkwXQtiaAadead",
		"VZ MSG: Congratulations! You’ve been selected for a special offer. Trade-in your old phone and get up to $1000 off the new iPhone 14. Visit m.vzpromotions.com/iphone14 to learn more. (data charges may apply).",
		"Friendly reminder from your dentist: Your bi-annual cleaning is due next week. Please call our office at 555-1234 to schedule an appointment.",
		"Friendly reminder from your dentist: Your bi-annual cleaning is due next week. Please call our office at 555-1234 to schedule an appointment.",
	}
	
	t.ExecuteTemplate(w, "slayout", nav)
	t.ExecuteTemplate(w, "templates", templates)
	t.ExecuteTemplate(w, "elayout", nil)

}

func aboutUsHandler(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseGlob("tmpl/*.html"))

	nav := []NavbarContext{
		{Href: "/home", Name: "Home"},
		{Href: "/templates", Name: "Templates"},
		{Href: "/about-us", Name: "About Us"},
	}

	t.ExecuteTemplate(w, "slayout", nav)
	t.ExecuteTemplate(w, "aboutus", nil)
	t.ExecuteTemplate(w, "elayout", nil)
}



func main() {
	// convert port to string
	port := strconv.Itoa(PORT)

	// running a file
	fmt.Printf("Server running: http://localhost:%s\n", port)

	// web server starts at this directory
	fs := http.FileServer(http.Dir("./"))
	http.Handle("/assets/", http.StripPrefix("/assets", fs))

	// Function Handlers
	http.HandleFunc("/", handler)
	http.HandleFunc("/templates", templatesHandler)
	http.HandleFunc("/about-us", aboutUsHandler)
	//http.HandleFunc("/investment", investmentHandler)
	//http.HandleFunc("/sms", smsHandler)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
