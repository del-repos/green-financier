package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/twilio/twilio-go"
	twilioApi "github.com/twilio/twilio-go/rest/api/v2010"
	"github.com/twilio/twilio-go/twiml"
	"log"
	"net/http"
	"os"
	"strconv"
	"text/template"
)

const PORT = 8080

type Sample_View struct {
	Href  string
	Title string
}

// handler: basic handler for the web server
func handler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("HackWeb.html")
	sampView := []Sample_View{{Href: "#", Title: "Doctor Portal"}}
	t.Execute(w, sampView)
}

func api(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		fmt.Fprintf(w, "no")
	case "POST":
		fmt.Fprintf(w, "yes")
	default:
		fmt.Fprintf(w, "No support yet!")
	}
}

func sendBasicSms(message, phone_num string) {
	m_phone := os.Getenv("M_PHONE")

	client := twilio.NewRestClient()

	params := &twilioApi.CreateMessageParams{}
	params.SetTo(phone_num)
	params.SetFrom(m_phone)
	params.SetBody(message)

	resp, err := client.Api.CreateMessage(params)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(resp)

}

// this shit don't work 
func smsHandler(w http.ResponseWriter, r *http.Request) {
	message := &twiml.MessagingMessage{
		Body: "Bruh fuck you",
	}

	// twimlRes := twiml.NewMessagingResponse(message)
	twimlRes, err := twiml.Messages([]twiml.Element{message})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "text/xml")
	w.Write([]byte(twimlRes))
}

func main() {
	// load .env file
	err := godotenv.Load()
	if err != nil {
		log.Fatal("err: no .env file found")
	}

	msg := os.Getenv("T_MSG")
	num := os.Getenv("T_PHONE")
	sendBasicSms(msg, num)

	// convert port to string
	port := strconv.Itoa(PORT)

	// running a file
	fmt.Printf("Server running: http://localhost:%s\n", port)

	// web server starts at this directory
	fs := http.FileServer(http.Dir("./"))

	http.Handle("/assets/", http.StripPrefix("/assets", fs))

	http.HandleFunc("/", handler)
	http.HandleFunc("/sms", smsHandler)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
